package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.mygdx.game.helpers.AssetLoader;

public class AppGame extends Game {


    @Override
    public void create() {
        AssetLoader.load();
        setScreen(new GameScreen(this));
    }


    @Override
    public void render() {
        super.render();
    }


    @Override
    public void dispose() {
        super.dispose();
        AssetLoader.dispose();
    }

}