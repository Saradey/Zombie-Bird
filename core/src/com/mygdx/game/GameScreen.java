package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.mygdx.game.gameworld.GameRenderer;
import com.mygdx.game.gameworld.GameWorld;
import com.mygdx.game.helpers.InputHandler;

/**
 * @author jtgn on 25.07.2018
 */

//В нашем GameScreen, мы делегируем обновление и отрисовку нашим классам
// GameWorld и GameRenderer, соответственно.


public class GameScreen implements Screen {

    final AppGame appGame;

    private GameWorld world;
    private GameRenderer renderer;

    private float runTime = 0;  //сколько времени игры прошло


    public GameScreen(final AppGame appGame) {
        float screenWidth = Gdx.graphics.getWidth();
        float screenHeight = Gdx.graphics.getHeight();
        float gameWidth = 136;
        float gameHeight = screenHeight / (screenWidth / gameWidth);

        System.out.println(gameHeight);

        int midPointY = (int) (gameHeight / 2);


        this.appGame = appGame;
        world = new GameWorld(midPointY); // initialize world
        renderer = new GameRenderer(world, (int) gameHeight, midPointY); // initialize renderer

        Gdx.input.setInputProcessor(new InputHandler(world.getBird()));
    }


    @Override
    public void render(float delta) {

        //Float delta это количество секунд (обычно очень маленькое значение)
        // которое прошло после последнего запуска метода render.
        //Gdx.app.log("GameScreen FPS", (1/delta) + "");

        //Во-первых, мы будем обновлять все наши игровые объекты.
        // Во-вторых, мы будет отрисовывать эти объекты.

        // Мы передаем delta в update метод, для того, чтобы мы могли сделать фреймо-зависимые вычисления
        runTime += delta;

        world.update(delta); // GameWorld updates
        renderer.render(runTime); // GameRenderer renders
    }

    @Override
    public void resize(int width, int height) {
        Gdx.app.log("GameScreen", "resizing");
    }

    @Override
    public void show() {
        Gdx.app.log("GameScreen", "show called");
    }

    @Override
    public void hide() {
        Gdx.app.log("GameScreen", "hide called");
    }

    @Override
    public void pause() {
        Gdx.app.log("GameScreen", "pause called");
    }

    @Override
    public void resume() {
        Gdx.app.log("GameScreen", "resume called");
    }

    @Override
    public void dispose() {
        // Leave blank
    }

}
