package com.mygdx.game.gameobjects;

import com.badlogic.gdx.math.Vector2;

/**
 * @author jtgn on 26.07.2018
 */
public class Bird {

    private Vector2 position;
    private Vector2 velocity; //отвечает за скорость по оси Y
    private Vector2 acceleration; //ускорение

    private float rotation; // Для обработки поворота птицы
    private int width;
    private int height;


    public Bird(float x, float y, int width, int height) {
        this.width = width;
        this.height = height;
        position = new Vector2(x, y);
        velocity = new Vector2(0, 0);
        acceleration = new Vector2(0, 460);
    }


    //Если ваша игра, по каким-то причинам, начнем тормозить, ваша delta увеличится
    public void update(float delta) {

        //Vector2.cpy() создает новый экземляр типа Vector2
        velocity.add(acceleration.cpy().scl(delta));

        if (velocity.y > 200) {
            velocity.y = 200;
        }

        position.add(velocity.cpy().scl(delta));


        if (velocity.y < 0) {
            rotation -= 600 * delta;

            if (rotation < -20) {
                rotation = -20;
            }
        }

        // Повернуть по часовой стрелке
        if (isFalling()) {
            rotation += 480 * delta;
            if (rotation > 90)
                rotation = 90;
        }

    }


    public boolean isFalling() { //следует ли повернуть птицу вниз.
        return velocity.y > 110;
    }

    public boolean shouldntFlap() { //когда наша птица должна перестать махать крыльями
        return velocity.y > 70;
    }


    public void onClick() {
        velocity.y = -140;
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getRotation() {
        return rotation;
    }

}
