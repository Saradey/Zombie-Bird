package com.mygdx.game.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.game.gameobjects.Bird;
import com.mygdx.game.helpers.AssetLoader;

/**
 * @author jtgn on 25.07.2018
 */
public class GameRenderer {

    private GameWorld world;

    private OrthographicCamera cam; //плоская камера
    private SpriteBatch batcher;

    private int midPointY;
    private int gameHeight;

    private Animation birdAnimation;

    private ShapeRenderer shapeRenderer;
    private TextureRegion birdRegion;

    private Bird bird;

    public GameRenderer(GameWorld world, int gameHeight, int midPointY) {
        this.world = world;

        // мы задаем значения параметрам класса
        // полченные из GameScreen.
        this.gameHeight = gameHeight;
        this.midPointY = midPointY;

        cam = new OrthographicCamera();
        cam.setToOrtho(true, 136, gameHeight);
        //Хотим ли использовать Orthographic проекцию (мы хотим)
        //Какая должна быть ширина
        //Какой должна быть высота

        batcher = new SpriteBatch();
        // привяжите batcher к камере
        batcher.setProjectionMatrix(cam.combined);

        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(cam.combined);

        initGameObject();
        initAssets();
    }

    private void initGameObject() {
        bird = world.getBird();
    }


    private void initAssets() {
        birdAnimation = AssetLoader.birdAnimation;
        birdRegion = AssetLoader.bird;
    }

    public void render(float runTime) {
// мы уберем это из цикла далее, для улучшения производительности
        bird = world.getBird();

        // Заполним задний фон одним цветом
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Стартуем ShapeRenderer
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        // Отрисуем Background цвет
        shapeRenderer.setColor(55 / 255.0f, 80 / 255.0f, 100 / 255.0f, 1);
        shapeRenderer.rect(0, 0, 136, midPointY + 66);

        // Отрисуем Grass
        shapeRenderer.setColor(111 / 255.0f, 186 / 255.0f, 45 / 255.0f, 1);
        shapeRenderer.rect(0, midPointY + 66, 136, 11);

        // Отрисуем Dirt
        shapeRenderer.setColor(147 / 255.0f, 80 / 255.0f, 27 / 255.0f, 1);
        shapeRenderer.rect(0, midPointY + 77, 136, 52);

        // Заканчиваем ShapeRenderer
        shapeRenderer.end();

        // Стартуем SpriteBatch
        batcher.begin();
        // Отменим прозрачность
        // Это хорошо для производительности, когда отрисовываем картинки без прозрачности
        batcher.disableBlending();
        batcher.draw(AssetLoader.bg, 0, midPointY + 23, 136, 43);

        // Птичке нужна прозрачность, поэтому включаем ее
        batcher.enableBlending();

        if (bird.shouldntFlap()) {
            batcher.draw(birdRegion, bird.getX(), bird.getY(),
                    bird.getWidth() / 2.0f, bird.getHeight() / 2.0f,
                    bird.getWidth(), bird.getHeight(), 1, 1, bird.getRotation());

        } else {
            batcher.draw((TextureRegion) birdAnimation.getKeyFrame(runTime), bird.getX(),
                    bird.getY(), bird.getWidth() / 2.0f,
                    bird.getHeight() / 2.0f, bird.getWidth(), bird.getHeight(),
                    1, 1, bird.getRotation());
        }


        // Заканчиваем SpriteBatch
        batcher.end();

    }


}
