package com.mygdx.game.gameworld;

import com.mygdx.game.gameobjects.Bird;

/**
 * @author jtgn on 25.07.2018
 */


//ы должны создавать все Game Objects внутри нашего GameWorld и отрисовывать их в GameRenderer.
public class GameWorld {

    private Bird bird;

    public GameWorld(int midPointY) {
        bird = new Bird(33, midPointY - 5, 17, 12);

    }

    public void update(float delta) {
        bird.update(delta);
    }

    public Bird getBird() {
        return bird;
    }


}

